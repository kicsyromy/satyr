use sdl2_sys::{
    SDL_GLContext, SDL_GL_CreateContext, SDL_GL_GetSwapInterval, SDL_GL_MakeCurrent,
    SDL_GL_SetSwapInterval, SDL_GL_SwapWindow, SDL_Window,
};

use crate::{
    rendering::{ContextError, RenderingError},
    Application, NuggetResult, WindowError,
};

// Shitty hack until I figure out why multiple contexts don't work with sokol_sgp
static mut THE_ONE_GL_CONTEXT: Option<SDL_GLContext> = None;

pub(crate) struct Context {
    context: SDL_GLContext,
    window_handle: *mut SDL_Window,
}

impl Context {
    pub fn new(
        window: &mut crate::Window,
        vsync: bool,
        _app: &mut Box<Application>,
    ) -> NuggetResult<Context> {
        let window_handle = window.native_handle() as *mut SDL_Window;

        let context = unsafe {
            if THE_ONE_GL_CONTEXT.is_some() {
                THE_ONE_GL_CONTEXT.as_ref().unwrap()
            } else {
                THE_ONE_GL_CONTEXT = Some(SDL_GL_CreateContext(window_handle));
                THE_ONE_GL_CONTEXT.as_ref().unwrap()
            }
        };

        if *context == std::ptr::null_mut() {
            let error_string = format!("Failed to create OpenGL context: {}", sdl_error!());
            return Err(
                WindowError::from(RenderingError::from(ContextError::NativeContextError(
                    error_string,
                )))
                .into(),
            );
        }

        let mut this = Context {
            context: context.clone(),
            window_handle,
        };
        this.set_vsync(vsync)?;

        Ok(this)
    }

    pub fn vsync(&self) -> bool {
        let result = unsafe { SDL_GL_GetSwapInterval() } != 0;
        result
    }

    pub fn set_vsync(&mut self, value: bool) -> NuggetResult<()> {
        self.activate()?;

        let res = unsafe { SDL_GL_SetSwapInterval(if value { -1 } else { 0 }) };
        if res != 0 {
            return Err(
                WindowError::from(RenderingError::from(ContextError::VSyncUpdateError(
                    sdl_error!().into(),
                )))
                .into(),
            );
        }

        Ok(())
    }

    pub fn activate(&mut self) -> NuggetResult<()> {
        let res = unsafe { SDL_GL_MakeCurrent(self.window_handle, self.context) };
        if res != 0 {
            Err(
                WindowError::from(RenderingError::from(ContextError::ActivationError(
                    sdl_error!().into(),
                )))
                .into(),
            )
        } else {
            Ok(())
        }
    }

    pub fn present(&mut self) {
        unsafe { SDL_GL_SwapWindow(self.window_handle) };
    }
}

impl Default for Context {
    fn default() -> Self {
        Self {
            context: std::ptr::null_mut(),
            window_handle: std::ptr::null_mut(),
        }
    }
}

// impl Drop for Context {
//     fn drop(&mut self) {
//         unsafe { SDL_GL_DeleteContext(self.context) }
//     }
// }
