use std::{
    error::Error,
    fmt::{Display, Formatter},
    sync::{PoisonError, RwLockReadGuard, RwLockWriteGuard},
};

pub use application_error::*;
pub use window_error::*;

use crate::Application;

mod application_error;
mod window_error;

#[derive(Debug, PartialEq)]
pub enum NuggetError {
    Application(ApplicationError),
    Window(WindowError),
}

impl From<ApplicationError> for NuggetError {
    fn from(e: ApplicationError) -> Self {
        Self::Application(e)
    }
}

impl From<WindowError> for NuggetError {
    fn from(e: WindowError) -> Self {
        Self::Window(e)
    }
}

impl From<PoisonError<RwLockWriteGuard<'_, Box<Application<'_>>>>> for NuggetError {
    fn from(e: PoisonError<RwLockWriteGuard<'_, Box<Application<'_>>>>) -> Self {
        e.into()
    }
}

impl From<PoisonError<RwLockReadGuard<'_, Box<Application<'_>>>>> for NuggetError {
    fn from(e: PoisonError<RwLockReadGuard<'_, Box<Application<'_>>>>) -> Self {
        e.into()
    }
}

impl Display for NuggetError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Application(e) => e.fmt(f),
            Self::Window(e) => e.fmt(f),
        }
    }
}

impl Error for NuggetError {}
