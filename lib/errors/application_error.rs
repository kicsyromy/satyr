/// Errors that can occur during application execution
use std::{
    fmt::{Debug, Display, Formatter},
    sync::{PoisonError, RwLockReadGuard, RwLockWriteGuard},
};

use crate::Application;

/// The errors that can occur during application execution
#[derive(Debug)]
pub enum ApplicationError {
    /// An instance of an application already exists and a new one cannot be created.
    AlreadyExists,
    /// Failed to initialize SDL.
    SDLInitializationFailed(String),
    /// GFX initialization failed
    GFXInitializationFailed(String),
    /// Cannot register for an event type
    EventRegistrationFailed(String),
    /// Failed to post an event to the queue
    PostEventFailed(String),
    /// A path could not be found
    PathNotFound(String),
    /// A path that was expected to be a directory was not
    NotADirectory(String),
    /// Not enough permissions to access a path
    PathPermissionDenied(String),
    /// An unknown system error
    SystemError(Box<dyn std::error::Error>),
}

impl From<PoisonError<RwLockWriteGuard<'_, Box<Application<'_>>>>> for ApplicationError {
    fn from(_: PoisonError<RwLockWriteGuard<'_, Box<Application<'_>>>>) -> Self {
        ApplicationError::SystemError("Application RwLock was poisoned, buggy app?".into())
    }
}

impl From<PoisonError<RwLockReadGuard<'_, Box<Application<'_>>>>> for ApplicationError {
    fn from(_: PoisonError<RwLockReadGuard<'_, Box<Application<'_>>>>) -> Self {
        ApplicationError::SystemError("Application RwLock was poisoned, buggy app?".into())
    }
}

impl PartialEq for ApplicationError {
    fn eq(&self, other: &Self) -> bool {
        match &self {
            ApplicationError::AlreadyExists => {
                if let ApplicationError::AlreadyExists = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::SDLInitializationFailed(_) => {
                if let ApplicationError::SDLInitializationFailed(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::GFXInitializationFailed(_) => {
                if let ApplicationError::GFXInitializationFailed(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::EventRegistrationFailed(_) => {
                if let ApplicationError::EventRegistrationFailed(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::PostEventFailed(_) => {
                if let ApplicationError::PostEventFailed(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::PathNotFound(_) => {
                if let ApplicationError::PathNotFound(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::NotADirectory(_) => {
                if let ApplicationError::NotADirectory(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::PathPermissionDenied(_) => {
                if let ApplicationError::PathPermissionDenied(_) = &other {
                    true
                } else {
                    false
                }
            }
            ApplicationError::SystemError(_) => {
                if let ApplicationError::SystemError(_) = &other {
                    true
                } else {
                    false
                }
            }
        }
    }
}

impl Display for ApplicationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ApplicationError::AlreadyExists => f.write_str(
                "ApplicationError: Cannot create more then once instance of Application",
            ),
            ApplicationError::SDLInitializationFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::GFXInitializationFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::EventRegistrationFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::PostEventFailed(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::PathNotFound(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::NotADirectory(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::PathPermissionDenied(message) => {
                f.write_fmt(format_args!("ApplicationError: {}", message))
            }
            ApplicationError::SystemError(error) => Display::fmt(&error, f),
        }
    }
}
