use derivative::Derivative;

use crate::events::{Event, EventCategory, EventType, KeyCode, MouseButton};

#[derive(Derivative)]
#[derivative(Debug)]
pub struct RenderEvent<'a> {
    time_elapsed: f32,
    frames_rendered: i64,
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> RenderEvent<'a> {
    pub fn new(
        time_elapsed: f32,
        frames_rendered: i64,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            time_elapsed,
            frames_rendered,
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }

    pub fn time_elapsed(&self) -> f32 {
        self.time_elapsed
    }

    pub fn frames_rendered(&self) -> i64 {
        self.frames_rendered
    }
}

impl Event for RenderEvent<'_> {
    fn event_type() -> EventType {
        EventType::Render
    }

    fn event_categories() -> EventCategory {
        EventCategory::RENDER
    }

    fn event_name() -> &'static str {
        "Render"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}
