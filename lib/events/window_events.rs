use derivative::Derivative;

use crate::events::{Event, EventCategory, EventType, KeyCode, MouseButton};

/// Window Events
///
/// Events that are triggered as a result of the window interactions triggered by the user,
/// by the operating system, or programmatically.

/// Event that signals that a window was shown after being obscured, minimized, or
/// otherwise loosing focus.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowShowEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowShowEvent<'a> {
    /// Constructs a new WindowShowEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowShowEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowShown
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowShown"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that a window was obscured, minimized, or otherwise lost focus.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowHideEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowHideEvent<'a> {
    /// Constructs a new WindowHideEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowHideEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowHidden
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowHidden"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that a window was closed by the user by pressing the close button, or
/// using an appropriate shortcut, etc.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowCloseEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowCloseEvent<'a> {
    /// Constructs a new WindowCloseEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowCloseEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowClosed
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowClosed"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that the mouse pointer is within the confines of the window client
/// area.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowMouseEnterEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowMouseEnterEvent<'a> {
    /// Constructs a new WindowMouseEnterEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowMouseEnterEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowMouseEntered
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowMouseEntered"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that the mouse pointer left the confines of the window client
/// area.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowMouseLeaveEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowMouseLeaveEvent<'a> {
    /// Constructs a new WindowMouseLeaveEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowMouseLeaveEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowMouseLeft
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowMouseLeft"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that the window has gained input focus.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowGainFocusEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowGainFocusEvent<'a> {
    /// Constructs a new WindowGainFocusEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowGainFocusEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowGainedFocus
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowGainedFocus"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that the window has lost input focus.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowLooseFocusEvent<'a> {
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowLooseFocusEvent<'a> {
    /// Constructs a new WindowLooseFocusEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowLooseFocusEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowLostFocus
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowLostFocus"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that the window has been moved either by the user or
/// programmatically.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowMoveEvent<'a> {
    /// The `x` coordinate of the event, with 0 representing the left-most point of the screen
    pub x: i32,
    /// The `y` coordinate of the event, with 0 representing the top-most point of the screen
    pub y: i32,

    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowMoveEvent<'a> {
    /// Constructs a new WindowMoveEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        x: i32,
        y: i32,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            x,
            y,
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowMoveEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowMoved
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowMoved"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}

/// Event that signals that a window's size has changed either by the user or
/// programmatically.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct WindowResizeEvent<'a> {
    /// The width of the window after resizing
    pub width: i32,
    /// The height of the window after resizing
    pub height: i32,

    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_position: (i32, i32),
}

impl<'a> WindowResizeEvent<'a> {
    /// Constructs a new WindowResizeEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        width: i32,
        height: i32,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
        mouse_position: (i32, i32),
    ) -> Self {
        Self {
            width,
            height,
            key_states,
            mouse_button_states,
            mouse_position,
        }
    }
}

impl Event for WindowResizeEvent<'_> {
    fn event_type() -> EventType {
        EventType::WindowResized
    }

    fn event_categories() -> EventCategory {
        EventCategory::WINDOW
    }

    fn event_name() -> &'static str {
        "WindowResized"
    }

    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }

    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }

    fn mouse_position(&self) -> (i32, i32) {
        self.mouse_position
    }
}
