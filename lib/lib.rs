pub use application::*;
use errors::*;
pub use window::*;

#[macro_use]
pub mod macros;

pub mod errors;
pub mod events;

mod application;
mod rendering;
mod window;

pub type NuggetResult<T> = Result<T, NuggetError>;

type DisableSync = std::marker::PhantomData<std::cell::Cell<()>>;
type DisableSend = std::marker::PhantomData<std::sync::MutexGuard<'static, ()>>;
