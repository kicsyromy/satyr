use num_traits::*;

use crate::graphics::{AngleUnit, DEGREES, RADIANS};

type Angle<F> = crate::graphics::Angle<RADIANS, F>;

pub struct Options<F: Float, I: PrimInt> {
    origin_pos: (F, F),
    start_tile: (I, I),

    origin_theta: Angle<F>,

    rays_per_degree: usize,
    ray_theta_increment: F,
    total_ray_count: usize,

    px_per_tile: F,
}

impl<F: Float, I: PrimInt> Default for Options<F, I> {
    fn default() -> Self {
        use crate::graphics::PI;

        let ten = unsafe { F::from(10).unwrap_unchecked() };
        let pi = unsafe { F::from(PI).unwrap_unchecked() };
        let one_eighty = unsafe { F::from(180).unwrap_unchecked() };

        Self {
            origin_pos: (F::zero(), F::zero()),
            start_tile: (I::zero(), I::zero()),

            origin_theta: Angle::zero(),

            rays_per_degree: 10,
            ray_theta_increment: (F::one() / ten) / (pi / one_eighty),
            total_ray_count: 0,

            px_per_tile: F::from(32).unwrap(),
        }
    }
}

pub struct OptionsBuilder<F: Float, I: PrimInt> {
    options: Options<F, I>,
    fov: crate::graphics::Angle<DEGREES, F>,
}

impl<F: Float, I: PrimInt> OptionsBuilder<F, I> {
    pub fn new() -> Self {
        Self { options: Default::default(), fov: F::from(45.0).expect("Failed to convert 45.0 to Float").to_radians().into() }
    }

    pub fn with_origin(mut self, x: F, y: F) -> Self {
        self.options.origin_pos = (x, y);
        self.options.start_tile = (I::from((self.options.origin_pos.0 / self.options.px_per_tile).trunc()).expect("Failed to calculate start tile"),
                                   I::from((self.options.origin_pos.1 / self.options.px_per_tile).trunc()).expect("Failed to calculate start tile"));
        self
    }

    pub fn with_theta<const ANGLE_UNIT: AngleUnit>(mut self, theta: crate::graphics::Angle<ANGLE_UNIT, F>) -> Self {
        self.options.origin_theta = theta.to_radians() - (self.fov.to_radians() / Angle::<F>::from_f32(2_f32));
        self
    }

    pub fn with_fov<const ANGLE_UNIT: AngleUnit>(mut self, theta: crate::graphics::Angle<ANGLE_UNIT, F>) -> Self {
        self.fov = theta.to_degrees();
        self.options.origin_theta = self.options.origin_theta - (theta.to_radians() / Angle::<F>::from_f32(2_f32));

        let rays_per_degree = F::from(self.options.rays_per_degree).expect("Failed to calculate theta increment: integer to float conversion failed");
        self.options.total_ray_count = num_traits::cast((self.fov.value() * rays_per_degree).trunc()).expect("Failed to calculate total ray count: float truncation error");

        self
    }

    pub fn with_rays_per_degree(mut self, v: usize) -> Self {
        use crate::graphics::PI;

        self.options.rays_per_degree = v;

        let v = F::from(v).expect("Failed to calculate theta increment: integer to float conversion failed");

        self.options.total_ray_count = num_traits::cast((self.fov.value() * v).trunc()).expect("Failed to calculate total ray count: float truncation error");
        self.options.ray_theta_increment = (F::one() / v).to_radians();

        self
    }

    pub fn with_px_per_tile(mut self, v: F) -> Self {
        self.options.px_per_tile = v;
        self.options.start_tile = (I::from((self.options.origin_pos.0 / v).trunc()).expect("Failed to calculate start tile"),
                                   I::from((self.options.origin_pos.1 / v).trunc()).expect("Failed to calculate start tile"));
        self
    }

    pub fn build(mut self) -> Options<F, I> {
        self.options.origin_theta = self.options.origin_theta.to_radians();
        self.options
    }
}

impl<F: Float, I: PrimInt> Options<F, I> {
    pub fn origin(&self) -> (F, F) {
        self.origin_pos
    }

    pub fn origin_x(&self) -> F {
        self.origin_pos.0
    }

    pub fn origin_y(&self) -> F {
        self.origin_pos.1
    }

    pub fn start_tile_x(&self) -> I {
        self.start_tile.0
    }

    pub fn start_tile_y(&self) -> I {
        self.start_tile.1
    }

    pub fn origin_theta(&self) -> Angle<F> {
        self.origin_theta
    }

    pub fn rays_per_degree(&self) -> usize {
        self.rays_per_degree
    }

    pub fn ray_theta_increment(&self) -> F {
        self.ray_theta_increment
    }

    pub fn px_per_tile(&self) -> F {
        self.px_per_tile
    }
}