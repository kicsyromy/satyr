use num_traits::*;

use crate::graphics::RADIANS;

type Angle<F> = crate::graphics::Angle<RADIANS, F>;

#[derive(Copy, Clone)]
pub struct RayInfo<Float: num_traits::Float> {
    // The coordinates within a tile oriented towards the ray's look direction
    start_offset_in_tile: (Float, Float),
    // The value to increment tiles, calculated based on ray direction and angle
    x_y_inc_sign: (i8, i8),
    // The angle used to calculate the length of the ray up to a particular tile
    normalized_theta: Angle<Float>,
}

impl<F: Float> RayInfo<F> {
    pub fn new(x_y: (F, F), theta: Angle<F>, px_per_tile: usize) -> Self {
        let mut this = Self::default();
        this.update(x_y, theta, px_per_tile);

        this
    }

    pub fn update(&mut self, x_y: (F, F), theta: Angle<F>, px_per_tile: usize) {
        let x_tile = x_y.0 / F::from(px_per_tile).unwrap();
        let y_tile = x_y.1 / F::from(px_per_tile).unwrap();

        let pi = Angle::<F>::pi();
        let pi_div_2 = Angle::<F>::pi_div_two();
        let three_pi_div_2 = Angle::<F>::three_pi_div_two();
        let two_pi = Angle::<F>::two_pi();

        // Quadrant 0
        if theta <= pi_div_2 && theta > Angle::<F>::zero() {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let theta = if theta == pi_div_2 {
                theta - Angle::<F>::epsilon()
            } else {
                theta
            };

            self.start_offset_in_tile = (F::one() - x_tile.fract(), F::one() - y_tile.fract());
            self.x_y_inc_sign = (1, 1);
            self.normalized_theta = theta;
        }
        // Quadrant 1
        else if theta <= pi && theta > pi_div_2 {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if theta == pi {
                theta - Angle::<F>::epsilon()
            } else {
                theta
            };

            self.start_offset_in_tile = (-F::one() * x_tile.fract(), F::one() - y_tile.fract());
            self.x_y_inc_sign = (-1, 1);
            self.normalized_theta = pi_div_2 - (angle - pi_div_2);
        }
        // Quadrant 2
        else if theta <= three_pi_div_2 && theta > pi {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if theta == three_pi_div_2 {
                theta - Angle::<F>::epsilon()
            } else {
                theta
            };

            self.start_offset_in_tile = (-F::one() * x_tile.fract(), -F::one() * y_tile.fract());
            self.x_y_inc_sign = (-1, -1);
            self.normalized_theta = angle - pi;
        }
        // Quadrant 3
        else {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if theta == two_pi {
                theta - Angle::<F>::epsilon()
            } else {
                theta
            };

            self.start_offset_in_tile = (F::one() - x_tile.fract(), -F::one() * y_tile.fract());
            self.x_y_inc_sign = (1, -1);
            self.normalized_theta = two_pi - angle;
        }
    }

    pub fn start_offset_x_in_tile(&self) -> F {
        self.start_offset_in_tile.0
    }

    pub fn start_offset_y_in_tile(&self) -> F {
        self.start_offset_in_tile.1
    }

    pub fn x_inc_sign(&self) -> F {
        F::from(self.x_y_inc_sign.0).unwrap()
    }

    pub fn y_inc_sign(&self) -> F {
        F::from(self.x_y_inc_sign.1).unwrap()
    }

    pub fn normalize(&self) -> Angle<F> {
        self.normalized_theta
    }
}

impl<Float: num_traits::Float> Default for RayInfo<Float> {
    fn default() -> Self {
        Self {
            start_offset_in_tile: (Float::zero(), Float::zero()),
            x_y_inc_sign: (0, 0),
            normalized_theta: Angle::zero(),
        }
    }
}
