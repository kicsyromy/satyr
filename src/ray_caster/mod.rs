use std::collections::HashSet;
use std::hash::Hash;

use num_traits::*;

use ray_info::*;

use crate::{
    graphics::RADIANS,
    Map,
    ray_caster::{
        options::Options,
        tile_map::TileMap,
    },
};

type Angle<F> = crate::graphics::Angle<RADIANS, F>;

mod ray_info;
mod options;
mod tile_map;

// A ray that has been cast
#[derive(Copy, Clone)]
pub struct CastRay<Float: num_traits::Float> {
    pub len: Float,
    pub player_delta_angle: Angle<Float>,
    pub hit_vertical_wall: bool,
    pub tile_intersection_point: Option<(Float, Float)>,
}

pub struct RayCaster<'a, F: Float, I: PrimInt + Hash, M: TileMap<Int=I>> {
    options: Options<F, I>,
    map: &'a M,

    start_theta: Angle<F>,
    index: usize,

    hit_test_fn: fn(I, I, F, F, &M) -> Option<(F, bool)>,

    // Data and structures used for debugging
    traversed_tiles: Option<&'a mut HashSet<(I, I)>>,
    cast_rays: Option<&'a mut Vec<CastRay<F>>>,
    add_traversed_tile_fn: fn(&mut Option<&mut HashSet<(I, I)>>, I, I),
    add_cast_ray_fn: fn(&mut Option<&mut Vec<CastRay<F>>>, ray: CastRay<F>),
    find_tile_intersection_point_fn: fn(F, usize, &RayInfo<F>, &Options<F, I>) -> Option<(F, F)>,
}

impl<'a, F: Float, I: PrimInt + Hash, M: TileMap<Int=I>> RayCaster<'a, F, I, M> {
    pub fn new(
        options: Options<F, I>,
        map: &'a M,
        traversed_tiles: Option<&'a mut HashSet<(I, I)>>,
        cast_rays: Option<&'a mut Vec<CastRay<F>>>,
        find_tile_intersection_point: bool,
    ) -> Self {
        let add_traversed_tile_fn = if traversed_tiles.is_some() {
            |hash_set: &mut Option<&mut HashSet<(I, I)>>, x: I, y: I| {
                let hash_set = hash_set.as_mut().unwrap();
                hash_set.insert((x, y));
            }
        } else {
            |_: &mut Option<&mut HashSet<(I, I)>>, _: I, _: I| {}
        };

        let add_cast_ray_fn = if cast_rays.is_some() {
            |rays: &mut Option<&mut Vec<CastRay<F>>>, ray: CastRay<F>| {
                let rays = rays.as_mut().unwrap();
                rays.push(ray);
            }
        } else {
            |_: &mut Option<&mut Vec<CastRay<F>>>, _: CastRay<F>| {}
        };

        let find_tile_intersection_point_fn = if find_tile_intersection_point {
            |ray_length: F, px_per_tile: usize, ray_info: &RayInfo<F>, options: &Options<F, I>| {
                let px_per_tile = F::from(px_per_tile).unwrap();
                let angle_opposite_distance =
                    (ray_length * px_per_tile) * ray_info.normalize().sin();
                let angle_adjacent_distance =
                    (ray_length * px_per_tile) * ray_info.normalize().cos();

                Some((
                    options.origin_x() + (ray_info.x_inc_sign() * angle_adjacent_distance),
                    options.origin_y() + (ray_info.y_inc_sign() * angle_opposite_distance),
                ))
            }
        } else {
            |_: F, _: usize, _: &RayInfo<F>, _: &Options<F, I>| None
        };

        let start_theta = options.origin_theta();
        Self {
            options,
            map,

            start_theta,
            index: 0,

            // Returns the ray length in grid units and if the ray hit a vertical wall
            hit_test_fn: |tile_x: I,
                          tile_y: I,
                          traversed_x: F,
                          traversed_y: F,
                          map: &M| {
                if map.can_ray_pass(tile_x, tile_y)
                {
                    Some(if traversed_x <= traversed_y {
                        (traversed_x, true)
                    } else {
                        (traversed_y, false)
                    })
                } else {
                    None
                }
            },

            cast_rays,
            traversed_tiles,
            add_traversed_tile_fn,
            add_cast_ray_fn,
            find_tile_intersection_point_fn,
        }
    }
}

impl<F: Float, I: PrimInt + Hash, M: TileMap<Int=I>> Iterator for RayCaster<'_, F, I, M> {
    type Item = CastRay<F>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.options.rays_per_degree() {
            return None;
        }

        let ray_info = RayInfo::new(
            self.options.origin(),
            self.start_theta.normalize(),
            self.map.px_per_tile(),
        );

        // https://www.math.net/sohcahtoa
        let ray_angle_sin = ray_info.normalize().sin();
        // The projected length of 1 tile unit, in the y axis, on the ray
        let ray_angle_inverse_sin = F::one() / ray_angle_sin;
        let ray_angle_cos = ray_info.normalize().cos();
        // The projected length of 1 tile unit, in the x axis, on the ray
        let ray_angle_inverse_cos = F::one() / ray_angle_cos;

        let (mut x, mut y) = (
            ray_info.start_offset_x_in_tile(),
            ray_info.start_offset_y_in_tile(),
        );
        let mut traversed_x = x.abs() / ray_angle_cos;
        let mut traversed_y = y.abs() / ray_angle_sin;

        let cast_ray;
        loop {
            let (traversed_x_prev, traversed_y_prev) = (traversed_x, traversed_y);

            if traversed_x <= traversed_y {
                x = x + ray_info.x_inc_sign();
                // The ray moves 1 unit in the x axis, add the projected value to the length of the ray
                traversed_x = traversed_x + ray_angle_inverse_cos;
            } else {
                y = y + ray_info.y_inc_sign();
                // The ray moves 1 unit in the y axis, add the projected value to the length of the ray
                traversed_y = traversed_y + ray_angle_inverse_sin
            }

            let tile_x = self.options.start_tile_x() + I::from(x.trunc()).unwrap();
            let tile_y = self.options.start_tile_y() + I::from(y.trunc()).unwrap();
            {
                if let Some((traversed_distance, hit_vertical_wall)) = (self.hit_test_fn)(
                    tile_x,
                    tile_y,
                    traversed_x_prev,
                    traversed_y_prev,
                    &self.map,
                ) {
                    cast_ray = CastRay {
                        len: traversed_distance,
                        hit_vertical_wall,
                        player_delta_angle: self.options.origin_theta() - self.start_theta,
                        tile_intersection_point: (self.find_tile_intersection_point_fn)(
                            traversed_distance,
                            self.map.px_per_tile(),
                            &ray_info,
                            &self.options,
                        ),
                    };
                    (self.add_cast_ray_fn)(&mut self.cast_rays, cast_ray.clone());

                    break;
                };

                (self.add_traversed_tile_fn)(&mut self.traversed_tiles, tile_x, tile_y);
            }
        }

        self.index += 1;
        self.start_theta = (self.start_theta.value() + self.options.ray_theta_increment()).into();

        Some(cast_ray)
    }
}

impl<Float: num_traits::Float> Default for CastRay<Float> {
    fn default() -> Self {
        Self {
            len: Float::zero(),
            player_delta_angle: Angle::from_f32(0_f32),
            hit_vertical_wall: false,
            tile_intersection_point: None,
        }
    }
}

