pub trait TileMap {
    type Int;

    fn width(&self) -> Self::Int;
    fn height(&self) -> Self::Int;

    fn px_per_tile(&self) -> usize;

    fn can_ray_pass(&self, tile_x: Self::Int, tile_y: Self::Int) -> bool;
}
