pub trait Map {
    fn tile_width(&self) -> i32;
    fn tile_height(&self) -> i32;
    fn px_per_tile(&self) -> u8;
}