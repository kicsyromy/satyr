use log::*;
use sokol_gp_sys::*;

use map::*;
use nugget::*;
use ray_caster::*;

mod graphics;
mod ray_caster;
mod map;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .try_init()
        .unwrap_or_default();

    let app = Application::new()?;

    #[allow(unused_variables)]
        let main_window = WindowBuilder::new()
        .width(1280)
        .height(720)
        .title("Satyr".into())
        .vsync(false)
        .on_key_pressed(enclose! { (app) move |window, key_press_event| { }})
        .on_mouse_button_pressed(enclose! { (app) move |window, mouse_btn_press_event| { }})
        .on_mouse_moved(enclose! { (app) move |_window, _mouse_move_event| { }})
        .on_render(enclose! { (app) move |window, render_event| {
             let (width, height) = window.size();

             unsafe {
                 sgp_begin(width, height);

                 sgp_viewport(0, 0, width, height);
                 sgp_project(0_f32, width as f32, 0_f32, height as f32);
             };

             unsafe {
                 sgp_set_color(0.1_f32, 0.1_f32, 0.1_f32, 1.0_f32);
                 sgp_clear();
             }

             unsafe {
                 let pass_action: sg_pass_action = core::mem::zeroed();
                 sg_begin_default_pass(&pass_action, width, height);
                 sgp_flush();
                 sgp_end();
                 sg_end_pass();
                 sg_commit();
             }
        }})
        .build()?;

    Application::exec(app)?;

    Ok(())
}
