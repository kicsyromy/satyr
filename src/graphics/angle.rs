use std::{
    ops::*,
};

use num_traits::*;

pub type AngleUnit = i8;

pub const DEGREES: AngleUnit = 0;
pub const RADIANS: AngleUnit = 1;

#[repr(transparent)]
#[derive(Copy, Clone, Debug, PartialOrd, PartialEq, Ord, Eq)]
pub struct Angle<const UNIT: AngleUnit, F: Float>(F);

impl<const UNIT: AngleUnit, F: Float> Angle<UNIT, F> {
    pub fn normalize(&self) -> Angle<RADIANS, F> {
        use super::TWO_PI;

        let two_pi = Angle::<RADIANS, F>(unsafe { F::from(TWO_PI).unwrap_unchecked() });
        let zero = Angle::<RADIANS, F>(F::zero());

        let this = self.to_radians();
        if this > two_pi {
            this - two_pi
        } else if this < zero {
            this + two_pi
        } else {
            this
        }
    }

    pub fn epsilon() -> F {
        F::epsilon()
    }

    pub fn zero() -> Self {
        Self::default()
    }

    pub fn one() -> Self {
        Self(unsafe { F::from(1).unwrap_unchecked() })
    }

    pub fn pi() -> Self {
        use super::{PI};

        unsafe { Self(F::from(PI).unwrap_unchecked()) }
    }

    pub fn pi_div_two() -> Self {
        use super::{PI_DIV_2};

        unsafe { Self(F::from(PI_DIV_2).unwrap_unchecked()) }
    }

    pub fn three_pi_div_two() -> Self {
        use super::{THREE_PI_DIV_2};

        unsafe { Self(F::from(THREE_PI_DIV_2).unwrap_unchecked()) }
    }

    pub fn two_pi() -> Self {
        use super::{TWO_PI};

        unsafe { Self(F::from(TWO_PI).unwrap_unchecked()) }
    }

    pub fn from_f32(v: f32) -> Self {
        Self(F::from(v).unwrap())
    }

    pub fn from_f64(v: f64) -> Self {
        Self(F::from(v).unwrap())
    }

    pub fn to_radians(&self) -> Angle<RADIANS, F> {
        if UNIT == DEGREES {
            Angle::<RADIANS, F>(self.0.to_radians())
        } else {
            Angle::<RADIANS, F>(self.0)
        }
    }

    pub fn to_degrees(&self) -> Angle<DEGREES, F> {
        use super::{PI};

        if UNIT == DEGREES {
            Angle::<DEGREES, F>(self.0)
        } else {
            Angle::<DEGREES, F>(self.0 * (unsafe { F::from(180_f32 / PI).unwrap_unchecked() }))
        }
    }

    pub fn sin(&self) -> F {
        self.to_radians().0.sin()
    }

    pub fn cos(&self) -> F {
        self.to_radians().0.cos()
    }

    pub fn value(&self) -> F {
        self.0
    }
}

impl<const UNIT: AngleUnit, F: Float> Default for Angle<UNIT, F> {
    fn default() -> Self {
        Angle(F::zero())
    }
}

impl<const UNIT: AngleUnit, F: Float> From<F> for Angle<UNIT, F> {
    fn from(v: F) -> Self {
        Angle(v)
    }
}

impl<const UNIT: AngleUnit, F: Float> Add for Angle<UNIT, F> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0)
    }
}

impl<const UNIT: AngleUnit, F: Float> Add<F> for Angle<UNIT, F> {
    type Output = Self;

    fn add(self, rhs: F) -> Self::Output {
        Self(self.0 + rhs)
    }
}

impl<const UNIT: AngleUnit, F: Float> Sub for Angle<UNIT, F> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self(self.0 - rhs.0)
    }
}

impl<const UNIT: AngleUnit, F: Float> Sub<F> for Angle<UNIT, F> {
    type Output = Self;

    fn sub(self, rhs: F) -> Self::Output {
        Self(self.0 - rhs)
    }
}

impl<const UNIT: AngleUnit, F: Float> Div for Angle<UNIT, F> {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        Self(self.0 / rhs.0)
    }
}

impl<const UNIT: AngleUnit> Div<f32> for Angle<UNIT, f32> {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        Self(self.0 / rhs)
    }
}

impl<const UNIT: AngleUnit> Div<f64> for Angle<UNIT, f64> {
    type Output = Self;

    fn div(self, rhs: f64) -> Self::Output {
        Self(self.0 / rhs)
    }
}
