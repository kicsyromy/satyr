pub const PI: f32 = std::f32::consts::PI;
pub const PI_DIV_2: f32 = PI / 2.0;
pub const THREE_PI_DIV_2: f32 = (3.0 * PI) / 2.0;
pub const TWO_PI: f32 = 2.0 * PI;
